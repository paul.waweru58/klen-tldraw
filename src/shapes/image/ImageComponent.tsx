import * as React from "react";
import { TLShapeUtil, HTMLContainer } from "@tldraw/core";
import type { ImageShape } from "./ImageShape";

import arsenal from "./invincibles.png";

export const ImageComponent = TLShapeUtil.Component<ImageShape, HTMLDivElement>(
  ({ shape, events, isGhost, meta }, ref) => {
    const color = meta.isDarkMode ? "white" : "black";
    const { id, size } = shape;

    return (
      <HTMLContainer ref={ref} {...events}>
        <div
          style={{
            pointerEvents: "all",
            width: size[0],
            height: size[1],
            display: "flex",
            fontSize: 20,
            fontFamily: "sans-serif",
            alignItems: "center",
            justifyContent: "center",
            color,
          }}
        >
          <img
            src={arsenal}
            style={{ pointerEvents: "none", width: size[0], height: size[1] }}
          />
        </div>
      </HTMLContainer>
    );
  }
);
