import { Utils, TLBounds } from '@tldraw/core'
import { intersectLineSegmentBounds } from '@tldraw/intersect'
import { nanoid } from 'nanoid'
import { CustomShapeUtil } from '../CustomShapeUtil'
import { ImageComponent } from './ImageComponent'
import { ImageIndicator } from './ImageIndicator'
import type { ImageShape } from './ImageShape'

type T = ImageShape
type E = HTMLDivElement

export class ImageUtil extends CustomShapeUtil<T, E> {
  Component = ImageComponent

  Indicator = ImageIndicator

  hideResizeHandles = false

  getBounds = (shape: T) => {
    const bounds = Utils.getFromCache(this.boundsCache, shape, () => {
      const [width, height] = shape.size

      return {
        minX: 0,
        maxX: width,
        minY: 0,
        maxY: height,
        width,
        height,
      } as TLBounds
    })

    return Utils.translateBounds(bounds, shape.point)
  }

  /* ----------------- Custom Methods ----------------- */

  canBind = true

  getShape = (props: Partial<T>): T => {
    return {
      id: nanoid(),
      type: 'image',
      name: 'Image',
      parentId: 'page1',
      point: [0, 0],
      size: [100, 100],
      childIndex: 1,
      ...props,
    }
  }

  shouldRender = (prev: T, next: T) => {
    return next.size !== prev.size
  }

  getCenter = (shape: T) => {
    return Utils.getBoundsCenter(this.getBounds(shape))
  }

  hitTestPoint = (shape: T, point: number[]) => {
    return Utils.pointInBounds(point, this.getBounds(shape))
  }

  hitTestLineSegment = (shape: T, A: number[], B: number[]) => {
    return intersectLineSegmentBounds(A, B, this.getBounds(shape)).length > 0
  }

  transform = (shape: T, bounds: TLBounds, initialShape: T, scale: number[]) => {
    shape.point = [bounds.minX, bounds.minY]
    shape.size = [bounds.width, bounds.height]
  }
}

// /* eslint-disable @typescript-eslint/no-non-null-assertion */
// import * as React from "react";
// import { Utils, HTMLContainer, TLBounds } from "@tldraw/core";
// import { TDShapeUtil } from "../TDShapeUtil";
// import { getStickyFontStyle, getStickyShapeStyle } from "../../shape-styles";
// import { Vec } from "@tldraw/vec";
// import { TDMeta, TDShapeType, TransformInfo } from "../../types";
// import { ImageShape } from "./ImageShape";
// import arsenal from "./invincibles.png";
// import { getBoundsRectangle } from "../../getBoundsRectangle";
// import { getTextSvgElement } from "../../getTextSvgElement";

// type T = ImageShape;
// type E = HTMLDivElement;

// // @ts-ignore
// export class ImageUtil extends TDShapeUtil<T, E> {
//   type = TDShapeType.Image as const;

//   canBind = true;

//   canEdit = true;

//   canClone = true;

//   hideResizeHandles = true;

//   showCloneHandles = true;

//   getShape = (props: Partial<T>): T => {
//     return Utils.deepMerge<T>(
//       {
//         id: "id",
//         type: TDShapeType.Image,
//         name: "Sticky",
//         parentId: "page",
//         childIndex: 1,
//         point: [0, 0],
//         size: [200, 200],
//         rotation: 0,
//       },
//       props
//     );
//   };

//   Component = TDShapeUtil.Component<T, E, TDMeta>(
//     (
//       { shape, meta, events, isGhost, isEditing, onShapeBlur, onShapeChange },
//       ref
//     ) => {
//       const rContainer = React.useRef<HTMLDivElement>(null);

//       const { id, size } = shape;

//       const rTextArea = React.useRef<HTMLTextAreaElement>(null);

//       const rText = React.useRef<HTMLDivElement>(null);

//       const rIsMounted = React.useRef(false);

//       const handlePointerDown = React.useCallback((e: React.PointerEvent) => {
//         e.stopPropagation();
//       }, []);

//       // Focus when editing changes to true
//       React.useEffect(() => {
//         if (isEditing) {
//           rIsMounted.current = true;
//           const elm = rTextArea.current!;
//           elm.focus();
//           elm.select();
//         }
//       }, [isEditing]);

//       return (
//         <HTMLContainer ref={ref} {...events}>
//           <div
//             style={{
//               pointerEvents: "all",
//               width: size[0],
//               height: size[1],
//               display: "flex",
//               fontSize: 20,
//               fontFamily: "sans-serif",
//               alignItems: "center",
//               justifyContent: "center",
//               color: "white",
//             }}
//           >
//             <img
//               src={arsenal}
//               style={{ pointerEvents: "all", width: size[0], height: size[1] }}
//             />
//           </div>
//         </HTMLContainer>
//       );
//     }
//   );

//   Indicator = TDShapeUtil.Indicator<T>(({ shape }) => {
//     const {
//       size: [width, height],
//     } = shape;

//     return (
//       <rect
//         x={0}
//         y={0}
//         rx={3}
//         ry={3}
//         width={Math.max(1, width)}
//         height={Math.max(1, height)}
//       />
//     );
//   });

//   getBounds = (shape: T) => {
//     return getBoundsRectangle(shape, this.boundsCache);
//   };

//   // shouldRender = (prev: T, next: T) => {
//   //   return (
//   //     next.size !== prev.size ||
//   //     next.style !== prev.style ||
//   //     next.text !== prev.text
//   //   );
//   // };

//   transform = (
//     shape: T,
//     bounds: TLBounds,
//     { scaleX, scaleY, transformOrigin }: TransformInfo<T>
//   ): Partial<T> => {
//     const point = Vec.toFixed([
//       bounds.minX +
//         (bounds.width - shape.size[0]) *
//           (scaleX < 0 ? 1 - transformOrigin[0] : transformOrigin[0]),
//       bounds.minY +
//         (bounds.height - shape.size[1]) *
//           (scaleY < 0 ? 1 - transformOrigin[1] : transformOrigin[1]),
//     ]);

//     return {
//       point,
//     };
//   };

//   transformSingle = (shape: T): Partial<T> => {
//     return shape;
//   };

//   getSvgElement = (shape: T): SVGElement | void => {
//     const bounds = this.getBounds(shape);
//     const textElm = getTextSvgElement(shape, bounds);
//     // @ts-ignore
//     const style = getStickyShapeStyle(shape.style);
//     textElm.setAttribute("fill", style.color);

//     const g = document.createElementNS("http://www.w3.org/2000/svg", "g");
//     const rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
//     rect.setAttribute("width", bounds.width + "");
//     rect.setAttribute("height", bounds.height + "");
//     rect.setAttribute("fill", style.fill);

//     g.appendChild(rect);
//     g.appendChild(textElm);

//     return g;
//   };
// }
