import type { TLShape } from '@tldraw/core'

export interface EditorShape extends TLShape {
  type: "editor";
  size: number[];
}
