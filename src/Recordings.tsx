import React, { useEffect } from "react";
import rrwebPlayer from "rrweb-player";

import "./index.css";

export const Recordings = () => {
  // @ts-ignore
  const events = JSON.parse(localStorage.getItem("domEvents"));

  useEffect(() => {
    launchRrweb(events);
  }, [events]);

  // @ts-ignore
  const launchRrweb = (events) => {
    new rrwebPlayer({
      // @ts-ignore
      target: document.getElementById("replayDiv"),
      props: {
        events,
        mouseTail: false,
        autoPlay: false,
      },
    });
  };

  return <div id="replayDiv" />;
};
