import * as React from "react";
import {
  ArrowUpRight,
  Edit2,
  MousePointer,
  Square,
  X,
  Image,
  Type,
  Play,
  PauseCircle,
  StopCircle,
} from "react-feather";
import { styled } from "../stitches-config";
import { machine } from "../state/machine";

import * as rrweb from "rrweb";

import "./index.css";

interface ToolbarProps {
  activeStates: string[];
  lastEvent: string;
}

const onToolSelect = (e: React.MouseEvent) => {
  machine.send("SELECTED_TOOL", { name: e.currentTarget.id });
};

// const onReset = () => {
//   machine.send("RESET");
// };

export function Toolbar({ activeStates, lastEvent }: ToolbarProps) {
  const [recordStart, setRecordStart] = React.useState(true);
  const [showStartButton, setShowStartButton] = React.useState(true);

  const [domEvents, setDomEvents] = React.useState([]);

  const emitEvent = () => {
    const _events = [...domEvents];
    return rrweb.record({
      emit(event) {
        console.log("recording event", event);
        // @ts-ignore
        _events.push(event);
        setDomEvents(_events);
      },
      recordCanvas: true,
    });
  };

  const startRecording = () => {
    setShowStartButton(false);
    console.log("started recording");
    setRecordStart(true);
    if (recordStart) {
      emitEvent();
    }
  };

  const stopRecording = () => {
    setShowStartButton(true);
    console.log("stopped recording");
    setRecordStart(false);

    localStorage.setItem("domEvents", JSON.stringify(domEvents));
    emitEvent();
    window.location.replace("/recordings");
  };

  return (
      <div className="rr-block">
    <ToolbarContainer>
        <PrimaryTools>
          <PrimaryToolButton
            id="select"
            isActive={machine.isIn("select")}
            onClick={onToolSelect}
          >
            <Highlight>
              <MousePointer />
            </Highlight>
          </PrimaryToolButton>
          <PrimaryToolButton
            id="eraser"
            isActive={machine.isIn("eraser")}
            onClick={onToolSelect}
          >
            <Highlight>
              <X />
            </Highlight>
          </PrimaryToolButton>
          <PrimaryToolButton
            id="pencil"
            isActive={machine.isIn("pencil")}
            onClick={onToolSelect}
          >
            <Highlight>
              <Edit2 />
            </Highlight>
          </PrimaryToolButton>
          <PrimaryToolButton
            id="box"
            isActive={machine.isIn("box")}
            onClick={onToolSelect}
          >
            <Highlight>
              <Square />
            </Highlight>
          </PrimaryToolButton>
          <PrimaryToolButton
            id="arrow"
            isActive={machine.isIn("arrow")}
            onClick={onToolSelect}
          >
            <Highlight>
              <ArrowUpRight />
            </Highlight>
          </PrimaryToolButton>
          <PrimaryToolButton
            id="image"
            isActive={machine.isIn("image")}
            onClick={onToolSelect}
          >
            <Highlight>
              <Image />
            </Highlight>
          </PrimaryToolButton>
          <PrimaryToolButton
            id="editor"
            isActive={machine.isIn("editor")}
            onClick={onToolSelect}
          >
            <Highlight>
              <Type />
            </Highlight>
          </PrimaryToolButton>
          {showStartButton === true ? (
            <PrimaryToolButton id="startRecording" onClick={startRecording}>
              <Highlight>
                <Play />
              </Highlight>
            </PrimaryToolButton>
          ) : (
            <PrimaryToolButton id="stopRecording" onClick={stopRecording}>
              <Highlight>
                <StopCircle />
              </Highlight>
            </PrimaryToolButton>
          )}
        </PrimaryTools>
    </ToolbarContainer>
      </div>
  );
}

const ToolbarContainer = styled("div", {
  display: "grid",
  gridTemplateColumns: "1fr",
  gridTemplateRows: "auto auto",
  gridRowGap: "$5",
  position: "fixed",
  bottom: "0",
  width: "100%",
  zIndex: "100",
});

const PrimaryTools = styled("div", {
  display: "flex",
  width: 500,
  borderRadius: "100px",
  border: "1px solid $border",
  overflow: "hidden",
  padding: "$2",
  justifySelf: "center",
  backgroundColor: "$background",
  justifyContent: "space-between",
});

const Highlight = styled("div", {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "100%",
  height: "100%",
  padding: 10,
  borderRadius: "100%",
  transition: "background-color .025s",
});

const PrimaryToolButton = styled("button", {
  cursor: "pointer",
  width: "40px",
  height: "40px",
  padding: 2,
  margin: 0,
  background: "none",
  backgroundColor: "none",
  border: "none",
  color: "$text",

  variants: {
    isActive: {
      true: {
        color: "$background",
        [`& > ${Highlight}`]: {
          backgroundColor: "$text",
        },
      },
      false: {
        [`&:hover > ${Highlight}`]: {
          backgroundColor: "$hover",
        },
        "&:active": {
          color: "$background",
        },
        [`&:active > ${Highlight}`]: {
          backgroundColor: "$text",
        },
      },
    },
  },
});

const StatusBar = styled("div", {
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
  padding: "8px 12px",
  borderTop: "1px solid $border",
  fontSize: "$1",
  fontWeight: "$1",
  backgroundColor: "$background",
  overflow: "hidden",
  whiteSpace: "nowrap",

  "& button": {
    background: "none",
    border: "1px solid $text",
    borderRadius: 3,
    marginRight: "$3",
    fontFamily: "inherit",
    fontSize: "inherit",
    cursor: "pointer",
  },
});
