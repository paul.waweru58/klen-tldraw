import type { TLPointerInfo } from '@tldraw/core'
import { Action } from '../../constants'

export const setHoveredShape: Action = (data, payload: TLPointerInfo) => {
  data.pageState.hoveredId = payload.target
}
