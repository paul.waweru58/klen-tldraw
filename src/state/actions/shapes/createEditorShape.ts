import { TLBoundsCorner, TLPointerInfo } from '@tldraw/core'
import { shapeUtils } from '../../../shapes'
import { Action } from '../../constants'
import { mutables } from '../../mutables'

export const createEditorShape: Action = (data, payload: TLPointerInfo) => {
  const shape = shapeUtils.editor.getShape({
    parentId: 'page1',
    point: mutables.currentPoint,
    size: [1, 1],
    childIndex: Object.values(data.page.shapes).length,
  })

  data.page.shapes[shape.id] = shape
  data.pageState.selectedIds = [shape.id]

  mutables.pointedBoundsHandleId = TLBoundsCorner.BottomRight
}
