import React from "react";
import "./index.css";

import { Routes, Route } from "react-router-dom";
import Canvas from "./Canvas";
import { Recordings } from "./Recordings";

export const App = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Canvas />} />
        <Route path="/recordings" element={<Recordings />} />
      </Routes>
    </div>
  );
};
